package org.chat;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.workgroup.agent.AgentSession;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * This class consist of Global contants
 * @author rahul.a
 */
public class GlobalUtils {

    static Connection connection;
    static MultiUserChat multiUserChat;
    static AgentSession agentSession;
    static String room;
    static boolean incoming_message_flag=true;
    static boolean accept_message_flag;

    static   List<MultiUserChat> multiuserchat= new ArrayList<MultiUserChat>();

    public static void setMultiuserchat(MultiUserChat muc) {
    
        multiuserchat.add(muc);

    }

    public static List<MultiUserChat> getMultiuserchat() {
        return multiuserchat;
    }

    
    

    
    public static void setAccept_message_flag(boolean accept_message_flag) {
        GlobalUtils.accept_message_flag = accept_message_flag;
    }

    public static boolean isAccept_message_flag() {
        return accept_message_flag;
    }

    public static void setIncoming_message_flag(boolean incoming_message_flag) {
        GlobalUtils.incoming_message_flag = incoming_message_flag;
    }

    public static boolean isIncoming_message_flag() {
        return incoming_message_flag;
    }

    public static void setMessage(String message) {
        GlobalUtils.message = message;
    }
    static String message;

    public static String getMessage() {
        return message;
    }

    public static String getRoom() {
        return room;
    }

    public static void setRoom(String room) {
        GlobalUtils.room = room;
    }

    public static AgentSession getAgentSession() {
        return agentSession;
    }

    public static Connection getConnection() {
        return connection;
    }

//    public static MultiUserChat getMultiUserChat() {
//        return multiUserChat;
//    }

    public static void setAgentSession(AgentSession agentSession) {
        GlobalUtils.agentSession = agentSession;
    }

    public static void setConnection(Connection connection) {
        GlobalUtils.connection = connection;
    }

    public static void setMultiUserChat(MultiUserChat multiUserChat) {
        GlobalUtils.multiUserChat = multiUserChat;
    }
}
