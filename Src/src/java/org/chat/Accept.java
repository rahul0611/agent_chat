package org.chat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jivesoftware.smack.Connection;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.MultiUserChat;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author rahul.a
 */
public class Accept implements InvitationListener {

    String message;


    List<MultiUserChat> multiuserchat= new ArrayList<MultiUserChat>();
    
    public void invitationReceived(Connection arg0, String arg1, String arg2, String arg3, String arg4, Message arg5) {
        try {
            System.out.println("OInvitation Recived");
          //  MultiUserChat muc = GlobalUtils.getMultiUserChat();
           MultiUserChat muc = new MultiUserChat(GlobalUtils.getConnection(), arg1);
            GlobalUtils.setRoom(arg1);
            multiuserchat.add(muc);
            muc.join("Agent");
            muc.addMessageListener(
                    new PacketListener() {

                        public void processPacket(Packet arg0) {

                          long time=System.currentTimeMillis();


                            System.out.println(((Message) arg0).getBody());


                            String from = arg0.getFrom();

                            System.out.println(from+"From");

                            message = ((Message) arg0).getBody();

                            GlobalUtils.setMessage(message+"#"+time+"#"+from);

                        }
                    });

            GlobalUtils.setMultiUserChat(muc);
        } catch (XMPPException ex) {
            Logger.getLogger(Accept.class.getName()).log(Level.SEVERE, null, ex);
        }


    }
}