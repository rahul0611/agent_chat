package org.chat;

import org.jivesoftware.smackx.workgroup.agent.Offer;
import org.jivesoftware.smackx.workgroup.agent.OfferListener;
import org.jivesoftware.smackx.workgroup.agent.RevokedOffer;

/**
 * This class is the offerListener implementation class , this will fire when the offer is beiung recived and when offer id being revoked
 *
 * @author rahul.a
 */
public class OfferListenereClass implements OfferListener {

    /**
     * This is called when offer is  received 
     * @param offer
     */
    public void offerReceived(Offer offer) {

        System.out.println("Offer recived");

        //  JOptionPane.showConfirmDialog(null, "Accept the incoimg request");


        GlobalUtils.setIncoming_message_flag(true);

        while (GlobalUtils.isAccept_message_flag()) {

           

        }

          offer.accept();

    }

    /**
     * This method is called when offer i revoked
     * @param offer
     */
    public void offerRevoked(RevokedOffer offer) {

        System.out.println("Offer revoked");

    }


    public void acceptOffer(String h){
 System.out.println("AcceptOffer#############################################################################");
    GlobalUtils.setAccept_message_flag(false);

        
    }


    public boolean isOffered(){

        return GlobalUtils.isIncoming_message_flag();


    }


    public void clearFlags(String h){


        GlobalUtils.setAccept_message_flag(true);
        
        GlobalUtils.setIncoming_message_flag(false);
    }


}
